import 'dotenv/config';
import jwt from 'jsonwebtoken';
import express from 'express';


const server = express();
server.use(express.json());
server.use(express.static('public'));

const payload = { username: 'Mystery' };
const secret = process.env.SECRET;
const options = { expiresIn: '1h'};
const verify = process.env.TOKEN;



const token = jwt.sign(payload, secret, options);
console.log(token);

const verification = jwt.verify(verify, secret);
console.log(verification);

const port = 3000;
server.listen(port, () => {
	console.log('Server listening port', port);
});




