import express from 'express';

import router2 from './userRouter';

const server = express();
server.use(express.json());
server.use('/',router2);
server.use(express.static('public'));


const port = 3000;
server.listen(port, () => {
	console.log('Server listening port', port);
});




