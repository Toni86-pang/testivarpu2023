import express, { Request,Response} from 'express';
import argon2 from 'argon2';

interface User {
    username: string,
    password: string
}
let users: Array<User> = [];

const router2 = express.Router();
router2.get('/', (_req: Request, res: Response) => {
	res.send(users.map(user => user.username));
});

router2.post('/register', (req: Request, res: Response) => {
	const { username , password } = req.body;
	argon2.hash(password)
		.then(result => console.log(result));

	users = users.concat({ username, password });
	console.log(users);
	res.status(201).send('done deal aka: Created');
});

router2.post('/login', (req: Request, res: Response) => {
	const { username , password } = req.body;
	users.map(user => user.username);
	argon2.verify(username ,password)  
		.then(result => (result ? res.status(204).send() : res.status(401).send()));
});


export default router2;