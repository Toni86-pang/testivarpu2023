import'dotenv/config';
import express, { Request,Response} from 'express';
import argon2 from 'argon2';
 

interface User {
    username: string,
    password: string
}
const users: Array<User> = [];

// { username: 'Reiska', password: 'Meikämandoliini' } 
//$argon2id$v=19$m=65536,t=3,p=4$vcpI1T/F5bT9Nj8aSCUpbg$i0V+iLFbydj8vRi0+TySGRcI9nRz106w4lgxMoeKd5U

const router2 = express.Router();


router2.post('/', (req: Request, res: Response) => {
	const { username , password } = req.body;
	const adminpsw = 'admin';
	argon2.hash(adminpsw)
		.then(result => console.log(result));
	const test = process.env.TEST ?? '';
	console.log(test,username,password);

	res.status(201).send('done deal aka: Created');
});

router2.post('/admin2', (req: Request, res: Response) => {
	const { username , password } = req.body;
	argon2.verify(username ,password.hash)  
		.then(result => (result ? res.status(204).send() : res.status(401).send()));
});


export default router2;