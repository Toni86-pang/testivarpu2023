import express, {Request,Response} from 'express';
import router2 from './userRouter';
import argon2 from 'argon2';
const server = express();
server.use(express.json());
server.use('/',router2);
server.use(express.static('public'));



server.post('/admin', async (req: Request, res: Response) => {
	const { username , password } = req.body;
	
	const adminuser = process.env.TEST ?? '';
	const adminpsw = 'admin';
	
	argon2.hash(adminpsw)
		.then(result => console.log(result));
	console.log(test,username,password);
	if(username !== adminuser) {
		return res.status(404).send();
	}

	const isCorretPassword = await argon2.verify(adminpsw,adminuser);
	if(!isCorretPassword) {
		return res.status(401).send('error');			
	}
	res.status(201).send('Ok');
});

const port = 3000;
server.listen(port, () => {
	console.log('Server listening port', port);
});




